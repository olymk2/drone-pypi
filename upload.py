#!/usr/bin/python
import os
import sys
import subprocess


PREFIX = "PLUGIN_"
PYPI_USERNAME = os.environ.get(f"{PREFIX}PYPI_USERNAME")
PYPI_PASSWORD = os.environ.get(f"{PREFIX}PYPI_PASSWORD")
PYPI_REPOSITORY = os.environ.get(
    f"{PREFIX}PYPI_REPOSITORY", "https://test.pypi.org/legacy/"
)

# LIVE url https://pypi.python.org/pypi
# TEST url https://test.pypi.org/legacy/
# sanity checks
if PYPI_USERNAME is None:
    sys.exit("No username supplied set PYPI_USERNAME")
if PYPI_PASSWORD is None:
    sys.exit("No password supplied set PYPI_PASSWORD")

# write out the .pyirc file with credentials
with open("/root/.pypirc", "w") as fp:
    fp.write(
        """
[distutils]
index-servers=
    pypi

[pypi]
repository:{url}
username:{username}
password:{password}
    """.format(
            **{
                "username": PYPI_USERNAME,
                "password": PYPI_PASSWORD,
                "url": PYPI_REPOSITORY,
            }
        )
    )

# run the actual pypi upload
print(f"Uploading to {PYPI_REPOSITORY} ...")
print(
    subprocess.call(
        ["python", "setup.py", "sdist", "upload", "-rpypi"], shell=True
    )
)
