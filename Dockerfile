# Docker image for drone to deploy package to pypi 

FROM python:3.6-alpine
RUN pip install twine
COPY ./upload.py /bin/upload
RUN chmod +x /bin/upload

ENTRYPOINT ["/bin/upload"]
